function crossedRule = crossOverRule(rule1,rule2)
% Cross rule 1 with rule 2, return rule 1 with parts of rule 2


% Choose # positions to cross
n=randi([1 length(rule1)]);

% Choose positions to mutate
for i=1:n
    positions(i)=randi([1 length(rule1)]);
end 

% Choose from rule 2, the guys crossover
for i=1:n
    crossover2(i)=randi([1 length(rule2)]);
end 

for i=1:n
    rule1(positions(i))=rule2(crossover2(i));
end
%return mutated rule
crossedRule=rule1;


end