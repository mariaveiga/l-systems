%comparison script

close all
%clear all

initPars = init_m();
algo = algo_m();

algo.productionRule=1;
algo.render=0;
initPars.maxGen=100;
initPars.destination=[25 50 50];
initPars.similarity=0.6;

for i=1:10
	[a,b,c] = runSimulationLsystemBADIDEA(initPars,algo);
    fitnessVector(i)=a;
    memoryUsedVector(i)=c;
    timeTakenVector(i)=b;
end

mean(fitnessVector)
mean(memoryUsedVector)
mean(timeTakenVector)

%%end

%figure
%plot((1:initPars.maxGen),fitnesses,'*')