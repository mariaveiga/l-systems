% Evolutionary Algorithm
%------------------------------
% INITIALIZATION
%------------------------------

clear all;
close all;

initPars = init_m(); %load properties of the problem
algo = algo_m(); %load properties of the algorithm

%generate the structures that hold the organisms
organism = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {}, 'rule', {});
rule = struct('before',{},'after',{});
mutate = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {}, 'rule', {});
elite = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {}, 'rule', {});
crossover = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {}, 'rule', {});

% INITIAL POPULATION
generation = 1;
pStart = tic;
i = 1;
while i <= initPars.nPopulation
    rule(i,:)=generateRule();
    gene=encodeLsystem(rule(i,:));
    good=genecheck(gene);
    if good~=1
        continue;
    end
    organism(generation,i)=buildMethod(gene,rule(i,:));
    
    if isempty(organism(generation,i).coordVec)==1
        continue;
    end
%    renderStructure(organism(generation,i),initPars.destination);
    
    [allright,maxStress,maxDisplacement,num_con] = checkOrgStability(organism(generation,i),initPars);
    organism(generation,i).maxStress = maxStress;
    organism(generation,i).maxDisplacement = maxDisplacement;
    organism(generation,i).numCon = num_con;
    organism(generation,i).gene = gene;
    %          if allright ~= 1
    %              continue;
    %          end
    %renderStructure(organism(generation,i),initPars.destination);
    i = i+1;
end

organism(generation,:) = checkfitness_m(initPars.nPopulation,organism(generation,:),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);
f = fitnessfunction_m(organism(generation,1),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);
%renderStructure(organism(generation,1),initPars.destination);

pEnd = toc(pStart);

disp([num2str(generation),'/',num2str(initPars.maxGen),'   ',num2str(f,'%7.4f'), '    Time: ', num2str(pEnd,'%6.2f')]);

plotFitness(generation)=f;

generation = generation + 1;
iTime = 0;
mTime = 0;
cTime = 0;

% Take the fittest rule and mutate it
% % ELITE, MUTATION, CROSS OVER
while f < 150 && generation<=initPars.maxGen
    iStart = tic;
    prev_gen(1:initPars.nPopulation) = organism(generation-1,:);

    
    %externalLoad = generation .* 0.03 .* [-1 1 -1];
    %what is this???
    
    % Elite
    elite(1:initPars.nElite) = prev_gen(1:initPars.nElite);
    i=1;
    % Mutation
    mStart = tic;
    while i <= initPars.nMutate
%        mut_rand = rand;
%        mutOrg = find(mut_rand <= cumsum(pdf_mut_mix),1);
        mutOrg = randi(size(prev_gen));
        mutatedRule=mutateRule(prev_gen(mutOrg).rule);
        mutatedGene=encodeLsystem(mutatedRule);
        good=genecheck(gene);
        if good~=1
            continue;
        end
        mutate(i) = buildMethod(mutatedGene,mutatedRule);
        %renderStructure(mutate(i));
        %[allright,maxStress,maxDisplacement,num_con] = checkOrgStability(organism(generation,i),initPars);
        %[allright,maxStress,maxDisplacement,num_con] = checkOrgStability(mutate(i),initPars);
        %mutate(i).maxStress = maxStress;
        %mutate(i).maxDisplacement = maxDisplacement;
        %mutate(i).numCon = num_con;
        %mutate(i).gene = gene;
        if isempty(mutate(i).coordVec)==1
            continue;
        end
        i = i+1;       
    end
    
    mEnd = toc(mStart);
    mTime = mTime + mEnd;
    
    % Cross over
    cStart = tic;
    j = 1;
    
    while j <= initPars.nCrossover
%        mut_rand = rand;
%        mutOrg = find(mut_rand <= cumsum(pdf_mut_mix),1);
        crossOrg = randi([1 size(prev_gen,2)],2);
%        mutatedRule=mutateRule(prev_gen(mutOrg).rule);
%        cross1 = randi(size(prev_gen));
        crossedRule = crossRules(prev_gen(crossOrg(1)).rule,prev_gen(crossOrg(2)).rule);
        mutatedGene=encodeLsystem(crossedRule);
        good=genecheck(gene);
        if good~=1
            continue;
        end
        crossover(j) = buildMethod(mutatedGene,mutatedRule);
        if isempty(crossover(j).coordVec)==1
            continue;
        end
%        renderStructure(crossover(i));
%        [allright,maxStress,maxDisplacement,num_con] = checkOrgStability(crossover(i),initPars);
%        crossover(i).maxStress = maxStress;
%        crossover(i).maxDisplacement = maxDisplacement;
%        crossover(i).numCon = num_con;
%       crossover(i).gene = gene;
        j = j+1;       
    end
    
    cEnd = toc(cStart);
    cTime = cTime + cEnd;
    organism(generation,:) = [elite, mutate, crossover];
    organism(generation,:) = checkfitness_m(initPars.nPopulation,organism(generation,:),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);
    organism(generation,:) = organism(generation,:);
    f = fitnessfunction_m(organism(generation,1),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);
%    renderStructure(organism(generation,1),initPars.destination);
    plotFitness(generation)=f;
    iEnd = toc(iStart);
    iTime = iTime + iEnd;
    cEnd=1;
    disp([num2str(generation),'/',num2str(initPars.maxGen),'   ',num2str(f,'%7.4f'),'    Mutation: ',num2str(mEnd,'%6.2f'),' Crossover: ',num2str(cEnd,'%6.2f'),' Iteration: ',num2str(iEnd,'%6.2f')]);
    generation = generation + 1;
    %
    if generation > initPars.maxGen
        break;
    end
    
end

disp (['Total Run Time: ',num2str(iTime,'%6.2f'),'   Mutation: ',num2str(mTime,'%6.2f'),'   Cross over: ',num2str(cTime,'%6.2f')]);
%plot fitness vs generation
xlabel('Generations')
ylabel('Fitness')
plot(1:(generation-1),plotFitness)
makeEvolutionMovie_m(organism,initPars)

