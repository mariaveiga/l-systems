function good = distanceMeasure(coordVecA,coordVecB)

same = intersect(coordVecA,coordVecB);

sizeA = size(coordVecA,1);
sizeB = size(coordVecB,2);

total = min(sizeA,sizeB);

ratio = same/total;

if ratio >= 0.8
	good = 0;	
else
	good = 1;
end 

end 
