function CrossedSetRules = crossRules2(setRules1, setRules2)

nSet1 = size(setRules1,2);
nSet2 = size(setRules2,2);

nChanges = randi([1 nSet1]);

whichChanges = datasample(1:nSet1,nChanges,'Replace',false);

for i = 1:nChanges
   setRules1(whichChanges(i)).after=setRules2(whichChanges(i)).after;
end

CrossedSetRules = setRules1;

end