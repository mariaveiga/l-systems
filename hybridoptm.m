%script for hybrid optimization
close all
%clear all

initPars = init_m();
algo = algo_m();

algo.render=0;
initPars.maxGen=60;
initPars.destination=[10 10 15];
iter=1;

for i=[0.2 0.4 0.6 0.8 1]
    hold off
    figure
    for j = 1:10
        initPars.similarity=i;
        [fitnesses, ~] = runSimulationNormal(initPars,algo);
        fitToAverage(iter,j)=fitnesses(end);
        hold on 
        str=['Fittest organism using similarity: %f', num2str(initPars.similarity)];
        title(str)
        xlabel('Generation #')
        ylabel('Fitness')
        plot((1:initPars.maxGen),fitnesses,'-')
    end
    iter=iter+1;
end

fitToAverage
mean(fitToAverage,2)