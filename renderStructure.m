function renderStructure(coordVec,target)
    
    figure
    axis xy
    axis on
    axis equal
    view(3);

    create_cube(coordVec(1,:),[0.1 0.1 0.1]);
    
    for i=2:size(coordVec,1)
        create_cube(coordVec(i,:),[0.7 0.7 0.7]);
    end
    
    create_cube(target,[0.5 0.5 0.1]);
    
    
end