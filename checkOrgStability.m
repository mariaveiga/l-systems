function [allright,maxStress,maxDisplacement,num_con] = checkOrgStability(organism,initPars)

% allright = 1 - good
% allright = 2 - stress in structure too high
% allright = 3 - stress between structure and ground too high
% allright = 4 - colision
% allright = 5 - obstacle
% allright = 6 - structure will tumble
% allright = 7 - collide with target

% check for stress
% check for collision
% check for tumbling
% check for collision with target

%disp('checking stability');

allright=0;
maxStress=0;
maxDisplacement=0;
num_con=0;

%disp('step 1: collision with obstacles');
allright = collisionCheck(organism.coordVec,initPars.obstacles);
 if allright~=1
     disp('Collides with obstacle');
     return;
 end

%disp('step 2: collision with target');
allright = collisionCheck(organism.coordVec, initPars.destination);
 if allright~=1
     disp('Collides with target');
     return;
 end

%disp('step 3: will it tumble from the table');
allright = tumbleCheck(organism.coordVec, initPars.tableSupport);
 if allright~=1
     disp('Will tumble');
     return;
  end

%disp('step 4: stress');
%pStart1 = tic;
 [allright, maxStress, maxDisplacement, num_con] = stressCheck(organism.coordVec,initPars);
 if allright~=1
     disp('Failed stress test');
     return;
 end
 %pEnd1 = toc(pStart1);
%pStart2 = tic; 
% [allright, maxStress, maxDisplacement, num_con]= stressCheckOrig(organism.coordVec,initPars);
%pEnd2 = toc(pStart2);
%disp(['Time sparse: ', num2str(pEnd1,'%6.2f'),' Time original: ', num2str(pEnd2,'%6.2f')]);


%   if allright~=1
%      return;
%   end

end