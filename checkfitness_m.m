function organism_gen = checkFitnessLsystem(nObject,organism_gen,destination,useDeflection,useStress,useCubes,useConnections)


%return a list of organisms ordered by fitness (Necessary?)

fitnessranking = zeros(nObject);

for i = 1:nObject
    fitnessranking(i) = fitnessfunction_m(organism_gen(i),destination,useDeflection,useStress,useCubes,useConnections);
end

% Rearrange all organisms so that the most fit gets number 1, the second
% best gets number 2, and so on...

for i = 1:nObject
    for j = 1:nObject
        if fitnessranking(j) < fitnessranking(i)
            temp = fitnessranking(i);
            fitnessranking(i) = fitnessranking(j);
            fitnessranking(j) = temp;
            temp_organism = organism_gen(i);
            organism_gen(i) = organism_gen(j);
            organism_gen(j) = temp_organism;
        end
    end
end
    
end