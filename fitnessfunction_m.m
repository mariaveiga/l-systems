function [fitness] = fitnessfunction_m(organism,destination,useDeflection,useStress,useCubes,useConnections)

maxStress = organism.maxStress;
maxDisplacement = organism.maxDisplacement;
numCon = organism.numCon;
% UPDATE 11.7. added displacement

nTarget = size(destination,1); % amount of destination points


n=length(organism.gene);
nElements=size(organism.coordVec,1);

distVec = zeros(nElements,nTarget);  % distance between a specific element and a
                                % specific target point
for target = 1:nTarget
    for element = 1:nElements
        distVec(element,target) = norm(organism.coordVec(element,:)-destination(target,:));
    end
end

minimalDistance = zeros(1,nTarget); % the minimal distance between an
                                    % element and a specific destination
                                    % point
relativeError = zeros(1,nTarget); % the relative error for each target
for target = 1:nTarget
    [~,minIndex] = min(distVec(:,target),[],1);
    minimalDistance(1,target) = distVec(minIndex,target);
    relativeError(1,target) = minimalDistance(1,target)/norm(destination(target,:));
end

criticalStress = 1.2e5; % [N/m^2]
% UPDATE 8.7. changed critStress from 1.2e6 to 1.2e5 

stressRatio = maxStress/criticalStress; % the less stress the organism
% experiences, the higher the fitness is

distanceFactor = 1e2;

%number of operations without adding a cube (work)
%should this be penalised?
nOperations = length(organism.gene)-nElements;
nOperationsWeight=0.1;

if useCubes
    weightFactor = 0.005; 
else
    weightFactor = 0;
end
if useStress
    stabilityFactor = 10; %1e-01;
else
    stabilityFactor = 0;
end
if useDeflection
    displacementFactor = 750; %1e2;
else
    displacementFactor = 0;
end
if useConnections
    connectionFactor = 0.01;
else
    connectionFactor = 0;
end


%accounts for the relative error and punishes high numbers of elements
fitness = distanceFactor*(1-sum(relativeError,2)/nTarget) - stabilityFactor*stressRatio - displacementFactor*maxDisplacement + connectionFactor*numCon - weightFactor*nElements ; %+ tumblingWeight*qq;

end