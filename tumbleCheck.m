function allright = tumbleCheck(coordVec, tableSupport)

nElements = size(coordVec,1);
allright=0;
% Find Base
coordVec = sortrows(coordVec,3);
% i=1;

% for i=1:nElements-1
%     while coordVec(i,3)==coordVec(i+1,3)
%         base(i,:) = coordVec(i,:);
%         i=i+1;
% end

% Calculate Center of Mass

cubeWeight = 1; % We assume cubes are homogeneous

centerOfMass=[sum(coordVec(:,1)) sum(coordVec(:,2)) sum(coordVec(:,3))]./nElements;
cOfMassProjection=[centerOfMass(1) centerOfMass(2)];

% Check if center of mass falls inside the table support
if cOfMassProjection(1)<=tableSupport(1,1) && cOfMassProjection(1)>=tableSupport(4,1)
    if cOfMassProjection(2)>=tableSupport(1,2) && cOfMassProjection(2)<=tableSupport(2,2)
        allright=1;
    end
end

end