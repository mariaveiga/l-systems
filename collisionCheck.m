%    renderStructure(organism(generation,i),initPars.destination);
function allright = collisionCheck(coordVec,obstacles)

% Obstacles are given as coordinates
% We want to check from 2 lists of vectors, if there is any intersection

allright = 0; %assume the worst

collisions = intersect(coordVec, obstacles, 'rows');

if size(collisions,1)==0
    allright = 1;
end 

end