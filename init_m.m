function initPars = init_m()
%--------------------------
% Initializes the problem: sets up the enviroment, defining the number of
% generations, number of cubes, number of organisms per generation
% Initializes the enviroment: sets up the obstacles, the target(s) and
% loads
%--------------------------

clear variables;
close all;

%Starting parameters:
initPars.maxGen = 100;                                   % maximum number of generations
initPars.nCubeMin = 25;                                  % minimum number of starting cubes
initPars.nCubeMax = 100;                                  % maximum number of starting cubes
initPars.nPopulation = 20;                               % size of the population
initPars.nElite = 1;                                     % size of the elite
initPars.mutationPercent = 0.6;                          % percentage of mutations wrt cross overs
initPars.maxOps=50;
initPars.similarity=0;

initPars.nMutate=round(initPars.mutationPercent*(initPars.nPopulation-initPars.nElite)); %number of mutations
initPars.nCrossover = initPars.nPopulation - initPars.nElite - initPars.nMutate;    % number of cross overs

% Enviroment 
initPars.destination=[0 10 8]; 
initPars.externalLoad = [0 5 10];                         % summation of all external forces
initPars.externalLoadPosition = initPars.destination;     % position of applied force

initPars.tableSupport=[ 30 -30 0 ; 30 30 0 ; -30 30 0 ; -30 -30 0 ]; 
initPars.obstacles=[0 0 -10];

%Defines the obstacles
% initPars.obstacles = [1 0 0; -1 0 0; 0 1 0; 0 -1 0; 1 1 0; -1 -1 0; 1 -1 0; -1 1 0;
%             1 0 1; -1 0 1; 0 1 1; 0 -1 1; 1 1 1; -1 -1 1; 1 -1 1; -1 1 1;
%             1 0 2; -1 0 2; 0 1 2; 0 -1 2; 1 1 2; -1 -1 2; 1 -1 2; -1 1 2;
%             1 0 -1; -1 0 -1; 0 1 -1; 0 -1 -1; 1 1 -1; -1 -1 -1; 1 -1 -1; 
%             -1 1 -1; 0 0 -1]; % around base

% initPars.obstacles =    [0 0 -1; 2 -2 -1; 2 -2 0; 2 -2 1; 2 -2 2; 2 -2 3;  
%                 2 -1 -1; 2 -1 0; 2 -1 1; 2 -1 2; 2 -1 3; 
%                 1 -2 -1; 1 -2 0; 1 -2 1; 1 -2 2; 1 -2 3;
%                 2 0 -1; 2 0 0; 2 0 1; 2 0 2; 2 0 3; 
%                 0 -2 -1; 0 -2 0; 0 -2 1; 0 -2 2; 0 -2 3;]; % wall

end