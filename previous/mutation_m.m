function mutatedgene = mutation_m(gene1)

% Receives a gene and decides the number of mutations and which
% mutations to do
% ---------
% --Method:
% ---------
%  1. Each instruction has a probability x to mutate
%  2. We choose a random set of points
%  3. Mutate

geneSize = size(gene1,1);

%randompt = randi(n1);
%mutatedgene=gene1;
maxIt=100;
good=0;
w=0;

while(good~=1 || w <= maxIt)
    mutatedgene=gene1; %go back to original mutation
    j=1;
    
    for i = 1:geneSize
        p=rand(1);
        if p<=0.2 %probability of mutation
            mutatedindex(j)=i;
            j=j+1;
        end
        
    end
    
    % We have the indices of mutation, for each index, we decide to either
    % switch to another instruction, remove it or duplicate it, all with equal
    % probability
    
    %in case we were not able to mutate anything
    if (exist('mutatedindex') == 0)
        mutatedindex=randi(geneSize);
    end
    
    for i=mutatedindex
        mutation = randi(3);
        switch mutation
            case{1}    %remove an arbitrary instruction
                mutatedgene(i)=0;
            case{2}    %repeat an arbitrary instruction
                mutatedgene1=[mutatedgene(1:i); mutatedgene(i); mutatedgene(i+1:end)];
                mutatedgene=mutatedgene1;
                
            case{3}    %transition to another guy
                swap=randi(6);
                mutatedgene(i)=swap;
        end
    end
    mutatedgene(mutatedgene==0) = []; %collapse the 0 entries
    
    good=genecheck(mutatedgene); %make a quick check to see if gene is buildable
    w=w+1;
end

end
