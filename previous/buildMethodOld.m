function organism = buildMethodOld(gene)
% Receives a vector with several instructions and generates the
% coordinateVector of the structure

% Checks for stability (whether it tumbles) every instructions
% It's blind against the enviroment it is in

geneLength=length(gene);

nCube=0;                % Find how many elements are used in the structure
for i = 1:geneLength
    if gene(i)==1
        nCube=nCube+1;
    end
end

maxStress = 0; % maximal stress in the structure
maxDisplacement = 0;
num_con = 0;
rule = 0;

% STABILITY QUICK CHECK
% param = paramst();

% Pre-allocate coordVec, initialize loops
j=1;
xCoord=0; yCoord=0; zCoord=0; % Start at the origin
elem=0;                       % Have 0 cubes placed
coordVec=zeros(nCube,3);

% Operations are as follow:
%     * add (1)
%     * shift right (y direction) (2)
%     * shift left(3) and
%     * shift along x direction positive (4)
%     * shift along x direction negative (5)

% Rules: can't shift twice
%        can't start with a shift.

while j <= geneLength
    if gene(j) == 1 %add a cube
        elem=elem+1;
        % We have to check if there is a cube below or not
        % yCoord
%        progress=coordVec(1:elem-1,:);
        zCoordMax=0;
        for i = 1:size(coordVec(1:elem-1,:),1)
            if [xCoord,yCoord]==coordVec(i,1:2) % if x, y are the same
                if (zCoordMax<=coordVec(i,3))
                    zCoordMax=coordVec(i,3);
                end
            end                                   % on top
        end    %parallelise I suppose we have to send chunks to different threads, determine local maximum and then determine global maximum
        
        zCoord=zCoordMax+1;
        coordVec(elem,:) = [xCoord, yCoord, zCoord];
    elseif gene(j) == 2
        coordVec = coordVec + repmat([0,1,0],nCube,1);
    elseif gene(j) == 3
        coordVec = coordVec + repmat([0,-1,0],nCube,1);
    elseif gene(j) == 4
        coordVec = coordVec + repmat([1,0,0],nCube,1);
    elseif gene(j) == 5
        coordVec = coordVec + repmat([-1,0,0],nCube,1);
    elseif gene(j) == 0
        disp('Zero instruction');
    else
        disp('Unknown instruction, skipping this');
    end
    j=j+1;
end

organism = struct('gene', gene, 'coordVec', coordVec, 'maxStress', maxStress,'maxDisplacement', maxDisplacement,'numCon', num_con);

end