function pdf_value = pdf (nMax)
% INPUT 
% nMax - number of organisms
% OUTPUT
% pdf_value - probability for choosing each organism from 1 to nMax

    x = 1:nMax;
    k = 3/(nMax-1)^3;
    y = k.*(x-nMax).^2;
    pdf_value = y./sum(y);
end
