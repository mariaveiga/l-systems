function covergene = mix_m(gene1,gene2)
% This function mixes two different organisms together. The mixing points
% on each organism determine the location where each gets cut. The
% remaining parts get merged together and form a new organism.

% UPDATE 6.7. add checkexisting

iter = 0;
iterMax = 50;
%while iter < 1

%--------------------------------------------------------------------------
% loading and preparing the mixed structure
%--------------------------------------------------------------------------
%     startVec = organismA.startVec;
%     parentVec = organismA.parentVec;
%     bdir = organismA.bdir;
%     nBranch = size(parentVec,1);
%     mSize = sum(cellfun('size',bdir,1));

good=0;

while(good~=1 || iter <=iterMax)
    size1=length(gene1);
    size2=length(gene2);
    
    randomPt1=randi(size1);
    randomPt2=randi(size2);
    
    covergene = zeros(randomPt1+(1+size2-randomPt2),1);
    
    covergene(1:randomPt1) = gene1(1:randomPt1);
    covergene((randomPt1+1):end) = gene2(randomPt2:end);
    
    good = genecheck(covergene);
    iter = iter + 1;
    
end