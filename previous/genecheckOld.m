function good = genecheck(gene)
% Pre check whether the gene is still good or not
% good=1, it's good

if gene(1)~=1
    good=0;
    return;
end

for i=2:length(gene)
   
   if gene(i)==2 && (gene(i-1)==2 || gene(i-1)==4 || gene(i-1)==5)
        good=0;
        return;
   elseif gene(i)==3 && (gene(i-1)==3 || gene(i-1)==4 || gene(i-1)==5)
        good=0;
        return;
   elseif gene(i)==4 && (gene(i-1)==2 || gene(i-1)==3)
        good=0;
        return;
   elseif gene(i)==5 && (gene(i-1)==2 || gene(i-1)==3)
        good=0;
        return;
   else 
   end

end

good=1;

end