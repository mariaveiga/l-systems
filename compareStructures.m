function similarity = compareStructures(A,B)

% receives coordinate vectors and compares them

%choose smallest structure as comparison
sizeA = size(A,1);
sizeB = size(B,1);

smallestone = min(sizeA,sizeB);

commonCoordinates = size(intersect(A,B,'rows'),1);

similarity = commonCoordinates/smallestone;

end