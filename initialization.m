close all
clear all

% load variables
initPars = init_m();
algo = algo_m();

% on the fly changes
algo.productionRule=2;
algo.render=0;
initPars.maxGen=100;
initPars.destination=[25 40 40];
initPars.similarity=0.4;
initPars.tableSupport=[ 30 -30 0 ; 30 30 0 ; -30 30 0 ; -30 -30 0 ];

for j = 1
    if algo.productionRule==1
        [fitnesses, ~] = runSimulationNormal(initPars,algo);
    elseif algo.productionRule==2
        [fitnesses, ~] = runSimulationLsystem(initPars,algo);
    end
    fitnessvector(j)=fitnesses(initPars.nPopulation);
end

% Plots
plot((1:initPars.maxGen),fitnesses,'*')
xlabel('# generations')
ylabel('fitness')
title('Target,', initPars.destination)
% makeEvolutionMovie_opt(organisms,initPars)