function initAlgo = algo_m()

% Options:
initAlgo.useProfiler = 0;     % 1 - use profiler, 0 - don't
initAlgo.useMovieMake = 0;    % 1 - make movie, 0 - don't
initAlgo.useParallel = 0;     % 1 - use parallel, 0 - don't
% if parallel is used then algorithm can't check for existing 

% Algorithm properties:
initAlgo.useStability = 1;    % 1 - stability gets checked, 0 - doesn't
initAlgo.useObstacles = 0;    % 1 - use obstacles, 0 - don't

%Fitness function parameters:
initAlgo.useDeflection = 1;  
initAlgo.useStress = 1;
initAlgo.useCubes = 1;
initAlgo.useConnections = 1;


%new add
initAlgo.render=0;

end