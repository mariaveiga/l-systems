function rule = mutateRule(initRule) %more parameters

% Only one type of mutation:
% Symbol mutation: Select the rule, mutate the symbol: change to a
% different instruction, add an instruction.

rule(1).before = initRule(1).before ;
%     rule(1).after = '1211';
%
rule(2).before = initRule(2).before;
%     rule(2).after = '11511';%

rule(3).before = initRule(3).before;
%     rule(3).after = '15113';
%
rule(4).before = initRule(4).before;
%     rule(4).after = '141151';
%
rule(5).before= initRule(5).before;
%     rule(5).after='141141';
%
%    disp('Function call')

for i = 1:5
    rule(i).after=initRule(i).after;
end

%select number of rules to change:
numRules=randi(5);

%select which rules to change
for i=1:numRules
    whichRule(i)=randi(5);
end

% select the mutation:
% change number 2
% add/remove number 3/4
for i=1:length(whichRule)
    s2='1';
    rule(whichRule(i)).after=strcat(rule(whichRule(i)).after,s2);
    j=randi(length(initRule(whichRule(i)).after));
    rule(whichRule(i)).after(j)=num2str(randi(5));
end

end