function [fitnesses, numgeneration, plottings] = runSimulationLsystem(initPars,algo)

%generate the structures that hold the organisms
organism = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {}, 'rule', {});
rule = struct('before',{},'after',{});
mutate = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {}, 'rule', {});
elite = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {}, 'rule', {});
crossover = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {}, 'rule', {});

% INITIAL POPULATION
generation = 1;
pStart = tic;

i = 1;
while i <= initPars.nPopulation
    rule(i,:)=generateRule();
    gene=encodeLsystem(rule(i,:));
    good=genecheck(gene);
    if good~=1
        continue;
    end
    [organism(generation,i).coordVec, organism(generation,i).gene]=buildMethodTest(gene);
    if isempty(organism(generation,i).coordVec)==1
        continue;
    end
    
    for numOrgs=1:i
       similarity = compareStructures(organism(generation,i).coordVec,organism(generation,numOrgs).coordVec);
       if similarity > initPars.similarity
           continue;
       end
    end
    
    [allright,maxStress,maxDisplacement,num_con] = checkOrgStability(organism(generation,i),initPars);
    organism(generation,i).maxStress = maxStress;
    organism(generation,i).maxDisplacement = maxDisplacement;
    organism(generation,i).numCon = num_con;
    organism(generation,i).rule = rule(i,:);

     if allright ~= 1
        continue;
     end
     
%     if algo.render == 1
%         renderStructure(organism(generation,i),initPars.destination);
%         str = sprintf('Specimen %d | Initial Generation',i);
%         title(str)
%     end
    i = i+1;
end

organism(generation,:) = checkfitness_m(initPars.nPopulation,organism(generation,:),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);
fitnesses(generation) = fitnessfunction_m(organism(generation,1),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);

if algo.render == 1
    renderStructure(organism(generation,1).coordVec,initPars.destination);
    str = sprintf('Fittest specimen | Initial Generation | Fitness %f',fitnesses(generation));
    title(str)
end

pEnd = toc(pStart);
disp([num2str(generation),'/',num2str(initPars.maxGen),'   ',num2str(fitnesses(generation),'%7.4f'), '    Time: ', num2str(pEnd,'%6.2f')]);

generation = generation + 1;
iTime = 0;
mTime = 0;
cTime = 0;

% Take the fittest rule and mutate it
% % ELITE, MUTATION, CROSS OVER
while generation<=initPars.maxGen
    iStart = tic;
    
    prev_gen(1:initPars.nPopulation) = organism(1,:);
    initPars.externalLoad = (generation).* 0.03 .* [-1 1 -1];
    
    plottings(generation-1,:)= organism(1,1);
        
    % Elite
    elite(1:initPars.nElite) = prev_gen(1:initPars.nElite);
    i=1;
    
    % Mutation
    mStart = tic;
    while i <= initPars.nMutate
        %        mut_rand = rand;
        %        mutOrg = find(mut_rand <= cumsum(pdf_mut_mix),1);
        mutOrg = randi(size(prev_gen));
        mutatedRule=workingMutationRule(prev_gen(mutOrg).rule);
        mutatedGene=encodeLsystem(mutatedRule);
        good=genecheck(gene);
        if good~=1
            continue;
        end
        [mutate(i).coordVec, mutate(i).gene] = buildMethodTest(mutatedGene);
        [allright,maxStress,maxDisplacement,num_con] = checkOrgStability(mutate(i),initPars);
        mutate(i).maxStress = maxStress;
        mutate(i).maxDisplacement = maxDisplacement;
        mutate(i).numCon = num_con;
        mutate(i).rule = mutatedRule;
                
        if isempty(mutate(i).coordVec)==1
            continue;
        end
        i = i+1;
    end
    
    mEnd = toc(mStart); mTime = mTime + mEnd;
    
    % Cross over
    cStart = tic;
    j = 1;
    while j <= initPars.nCrossover
        %mut_rand = rand;
        %mutOrg = find(mut_rand <= cumsum(pdf_mut_mix),1);
        crossOrg = randi([1 size(prev_gen,2)],2);
        crossedRule = crossRules2(prev_gen(crossOrg(1)).rule,prev_gen(crossOrg(2)).rule);
        mutatedGene=encodeLsystem(crossedRule);
        good=genecheck(gene);
        if good~=1
            continue;
        end
        [crossover(j).coordVec, crossover(j).gene] = buildMethodTest(mutatedGene);
        if isempty(crossover(j).coordVec)==1
            continue;
        end
        
        [allright,maxStress,maxDisplacement,num_con] = checkOrgStability(crossover(j),initPars);
        crossover(j).maxStress = maxStress;
        crossover(j).maxDisplacement = maxDisplacement;
        crossover(j).numCon = num_con;
%        crossover(j).gene = gene;
        crossover(j).rule = crossedRule;
% 
%         renderStructure(prev_gen(crossOrg(1)).coordVec,initPars.destination);
%         renderStructure(prev_gen(crossOrg(2)).coordVec,initPars.destination);
%         renderStructure(crossover(j).coordVec,initPars.destination);
%         title('crossover')

        
        
        j = j+1;
    end
    
    cEnd = toc(cStart);
    cTime = cTime + cEnd;
    organism(2,:) = [elite, mutate, crossover];
    organism(2,:) = checkfitness_m(initPars.nPopulation,organism(2,:),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);
    fitnesses(generation) = fitnessfunction_m(organism(2,1),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);
    iEnd = toc(iStart);
    iTime = iTime + iEnd;
    cEnd=1;
    
    if algo.render == 1
        renderStructure(organism(2,1).coordVec,initPars.destination);
        str = sprintf('Specimen %d | %d th Generation | Fitness: %f',i,generation,fitnesses(generation));
        title(str)
    end
    
    disp([num2str(generation),'/',num2str(initPars.maxGen),'   ',num2str(fitnesses(generation),'%7.4f'),'    Mutation: ',num2str(mEnd,'%6.2f'),' Crossover: ',num2str(cEnd,'%6.2f'),' Iteration: ',num2str(iEnd,'%6.2f')]);
    
    organism(1,:)=organism(2,:);
    
    generation=generation+1;
    if generation > initPars.maxGen
        break;
    end
    
end
   
plottings(generation-1,:)= organism(1,1);

disp (['Total Run Time: ',num2str(iTime,'%6.2f'),'   Mutation: ',num2str(mTime,'%6.2f'),'   Cross over: ',num2str(cTime,'%6.2f')]);
numgeneration=generation-1;
%makeEvolutionMovie_m(organism,initPars)
%makeEvolutionMovie_opt(plottings,initPars)
end