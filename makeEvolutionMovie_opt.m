function makeEvolutionMovie_opt(organism,param)

%% Prepare Movie
movieFig = figure(15);
set(gcf, 'Color','white')
set(gca, 'nextplot','replacechildren', 'Visible','off');

%# create AVI object
vidObj = VideoWriter('evolveFabStructure.avi');
vidObj.Quality = 100;
vidObj.FrameRate = 10;
open(vidObj);

%% Plot initial generation
% % Build structure
% for i=1:param.nPopulation
%     figure(movieFig);
%     renderStructure(organism(1,i).coordVec,param.destination);
%     str = sprintf('Specimen %f | Generation 1',i);
%     title(str)
%     axis vis3d
%     axis off
%     axis equal
%     view(-37.5,20);
%     writeVideo(vidObj, getframe(gcf));  
% end
%[locMat,~] = buildFabStructure(genes{1},target);

% Create movie frame

%% Loop through development
for i = 1:param.maxGen
    % Compare subsequent genes
        % Create movie frame
        figure(movieFig);
        renderStructure(organism(i,1).coordVec,param.destination);
        str = sprintf('Fittest specimen | Generation %f',i);
        title(str)
        axis vis3d
        axis off
        axis equal
        view(-37.5,20);
        writeVideo(vidObj, getframe(gcf));
end

%% Orbit
angleStep = 5;
for angle = 1:360/angleStep
    camorbit(angleStep,0);
    writeVideo(vidObj, getframe(gcf));
end

%% Finish Movie
for i=1:20
    writeVideo(vidObj, getframe(gcf));
end

% close(movieFig);
%# save as AVI file, and open it using system video player
close(vidObj);
winopen('evolveFabStructure.avi')
