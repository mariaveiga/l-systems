function [coordinates,gene2] = buildMethodTest(gene)
% Receives a vector with several instructions and generates the
% coordinateVector of the structure

% Checks for stability (whether it tumbles) every instructions
% It's blind against the enviroment it is in

geneLength=length(gene);

nCube=0;                % Find how many elements are used in the structure
for i = 1:geneLength
    if gene(i)==1
        nCube=nCube+1;
    end
end

%maxStress = 0; % maximal stress in the structure
%maxDisplacement = 0;
%num_con = 0;
%rule = 0;

% STABILITY QUICK CHECK
% param = paramst();

% Pre-allocate coordVec, initialize loops
j=1;
xCoord=0; yCoord=0; zCoord=0; % Start at the origin
elem=0;                       % Have 0 cubes placed
coordVec=zeros(nCube,4);
elemtemp=0;

% Operations are as follow:
%     * add (1)
%     * shift right (y direction) (2)
%     * shift left(3) and
%     * shift along x direction positive (4)
%     * shift along x direction negative (5)

% Rules: can't shift twice
%        can't start with a shift.
%coordVecTemp=zeros(1,4);
while j <= geneLength-1
    if gene(j) == 1 %add a cube
        elem=elem+1;
        % We have to check if there is a cube below or not
        zCoordMax=-1;
        for i = 1:size(coordVec(1:elem-1,:),1)
            if [xCoord,yCoord]==coordVec(i,2:3) % if x, y are the same
                if (zCoordMax<=coordVec(i,4))
                    zCoordMax=coordVec(i,4);
                end
            end                                   % on top
        end    %parallelise I suppose we have to send chunks to different threads, determine local maximum and then determine global maximum
        
        zCoord=zCoordMax+1;
        coordVec(elem,:) = [j , xCoord, yCoord, zCoord];
    elseif gene(j) == 2
        yCoord=yCoord-1;
        if gene(j+1)~= 1 && gene(j+1) ~=3
            nonEmpty=0;
            for i = 1:size(coordVec(1:elem,:),1)
                if [xCoord,yCoord]==coordVec(i,2:3) % if x, y are the same
                    % check if there's a cube there
                    nonEmpty = 1;
                end                                  
            end     
            if nonEmpty == 0
                elemtemp = elemtemp+1;
                zCoord=0;
                coordVecTemp(elemtemp,:) = [j , xCoord, yCoord, zCoord];
            end
        end
    elseif gene(j) == 3
        yCoord=yCoord+1;
        if gene(j+1)~= 1 && gene(j+1)~=2
            nonEmpty=0;
            for i = 1:size(coordVec(1:elem,:),1)
                if [xCoord,yCoord]==coordVec(i,2:3) % if x, y are the same
                    % check if there's a cube there
                    nonEmpty = 1;
                end                                   % on top
            end    %parallelise I suppose we have to send chunks to different threads, determine local maximum and then determine global maximum
            if nonEmpty == 0
                elemtemp = elemtemp+1;
                zCoord=0;
                coordVecTemp(elemtemp,:) = [j , xCoord, yCoord, zCoord];
            end
        end
        
    elseif gene(j) == 4
        xCoord=xCoord-1;
        if gene(j+1)~= 1 && gene(j+1)~=5
            % We have to check if there is a cube below or not
            nonEmpty=0;
            for i = 1:size(coordVec(1:elem,:),1)
                if [xCoord,yCoord]==coordVec(i,2:3) % if x, y are the same
                    % check if there's a cube there
                    nonEmpty = 1;
                end                                   % on top
            end    %parallelise I suppose we have to send chunks to different threads, determine local maximum and then determine global maximum
            if nonEmpty == 0
                elemtemp = elemtemp+1;
                zCoord=0;
                coordVecTemp(elemtemp,:) = [j , xCoord, yCoord, zCoord];
            end
        end
        
    elseif gene(j) == 5
        xCoord=xCoord+1;
        if gene(j+1)~= 1 && gene(j+1)~=4
            % We have to check if there is a cube below or not
            nonEmpty=0;
            for i = 1:size(coordVec(1:elem,:),1)
                if [xCoord,yCoord]==coordVec(i,2:3) % if x, y are the same
                    % check if there's a cube there
                    nonEmpty = 1;
                end                                   % on top
            end    %parallelise I suppose we have to send chunks to different threads, determine local maximum and then determine global maximum
            if nonEmpty == 0
                elemtemp = elemtemp+1;
                zCoord=0;
                coordVecTemp(elemtemp,:) = [j , xCoord, yCoord, zCoord];
            end
        end
        
    else
 %       disp('Unknown instruction, skipping this');
    end
    j=j+1;
end

% last entry
    if gene(geneLength) == 1 %add a cube
        elem=elem+1;
        zCoordMax=-1;
        for i = 1:size(coordVec(1:elem-1,:),1)
            if [xCoord,yCoord]==coordVec(i,2:3) % if x, y are the same
                if (zCoordMax<=coordVec(i,4))
                    zCoordMax=coordVec(i,4);
                end
            end                                   % on top
        end
        zCoord=zCoordMax+1;
        coordVec(elem,:) = [j , xCoord, yCoord, zCoord];
    elseif gene(geneLength) == 2
        yCoord=yCoord-1;
    elseif gene(geneLength) == 3
        yCoord=yCoord+1;
    elseif gene(geneLength) == 4
        xCoord=xCoord-1;
    elseif gene(geneLength) == 5
        xCoord=xCoord+1;       
    else
%        disp('Unknown instruction, skipping this');
    end

if exist('coordVecTemp','var')~=0
    coordVec=[coordVec; coordVecTemp];
    insert = @(p, b, k)cat(1,  b(1:k), p, b(k+1:end));
        for i=1:size(coordVecTemp,1)
        if exist('gene2','var')~=0
%             coordVecTemp(i,1)+i-1
            gene2 = insert(1,gene2,coordVecTemp(i,1)+i-1);
        else
%             gene
%             coordVecTemp(i,1)
%             gene(1:coordVecTemp(i,1))
%             gene(coordVecTemp(i,1)+1:end)
            gene2 = insert(1,gene,coordVecTemp(i,1));            
        end
        end
end

if exist('gene2','var')==0
    gene2=gene;
end

coordVec=sortrows(coordVec,1);
coordVec(:,2:3)=coordVec(:,2:3)-repmat([xCoord, yCoord],size(coordVec,1),1);

%give back the good gene

coordinates = coordVec(:,2:4);
%organism = struct('gene', gene, 'coordVec', coordVec(:,2:4), 'maxStress', maxStress,'maxDisplacement', maxDisplacement,'numCon', num_con, 'rule', rule);

end