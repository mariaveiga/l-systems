function gene = encodeLsystem(rule)

%get the number of rules
numRules = size(rule,2);

%starting seed
axiom = '1'; %start by adding a cube

%number of reps
nReps = 2;

for i=1:nReps
    
    %one character/cell, with indexes the same as original axiom string
    axiomINcells = cellstr(axiom');
    
    for j=1:numRules
        %Find the vector of indexes of each 'before' string for each rule
        hit = strfind(axiom, rule(j).before);
        if (length(hit)>=1)
            %set all of the hits to the new string
            %This does not vectorize
            for k=hit
                axiomINcells{k} = rule(j).after;
            end
        end
    end
    
    %now convert individual cells back to a string for the next repetition
    axiom=[];
    for j=1:length(axiomINcells)
        axiom = [axiom, axiomINcells{j}];
    end
end


axiom = strrep(axiom,'0','');
% %concatenate axiom into gene
gene = [];
for i=1:length(axiom)
  gene = [gene,' ',axiom(i)];
end

gene=str2num(gene);
gene=gene';