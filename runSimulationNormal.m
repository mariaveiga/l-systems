function [fitnesses, generations, plottings] = runSimulationNormal(initPars,algo)

obstacles = initPars.obstacles;

%generate the structures that hold the organisms
organism = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {});
mutate = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {});
elite = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {});
crossover = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {});

% probability density function used to calculate the probability of
% choosing a certain organism for mutation and cross over (first organism
% has the highest probability)
pdf_mut_mix = pdf(initPars.nPopulation);

% INITIAL POPULATION
generation = 1;
pStart = tic;
i = 1;
while i <= initPars.nPopulation
    r = rand;
    pdf_init = ones(1,initPars.nCubeMax-initPars.nCubeMin+1)*1/(initPars.nCubeMax-initPars.nCubeMin+1);
    nCube = find(r <= cumsum(pdf_init),1)+initPars.nCubeMin-1;
    gene = encode(nCube,initPars.maxOps,algo.useObstacles,obstacles);
    good = genecheckOld(gene);
    if good ~= 1
        return;
    end 
    organism(generation,i).coordVec=buildMethodTest(gene);
    
    for numOrgs=1:i
       similarity = compareStructures(organism(generation,i).coordVec,organism(generation,numOrgs).coordVec);
       if similarity > initPars.similarity
 %          disp('structures are too similar');
           continue;
       end
    end
    
    [allright,maxStress,maxDisplacement,num_con] = checkOrgStability(organism(generation,i),initPars);
    organism(generation,i).maxStress = maxStress;
    organism(generation,i).maxDisplacement = maxDisplacement;
    organism(generation,i).numCon = num_con;
    organism(generation,i).gene = gene;
    
    if allright ~= 1
        continue;
    end
    
%     %renderStructure(organism(generation,i),initPars.destination);
%     if algo.render == 1
%         renderStructure(organism(generation,i),initPars.destination);
%         str = sprintf('Specimen %d | Initial Generation',i);
%         title(str)
%     end
    
    i = i+1;
end

organism(generation,:) = checkfitness_m(initPars.nPopulation,organism(generation,:),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);
fitnesses(generation) = fitnessfunction_m(organism(generation,1),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);
pEnd = toc(pStart);
%disp([num2str(generation),'/',num2str(initPars.maxGen),'   ',num2str(fitnesses(generation),'%7.4f'), '    Time: ', num2str(pEnd,'%6.2f')]);

%render_m(organism(generation,1),initPars.destination);
generation = generation + 1;
iTime = 0;
mTime = 0;
cTime = 0;

% % ELITE, MUTATION, CROSS OVER
while fitnesses(generation-1) < 150 && generation<=initPars.maxGen
    iStart = tic;
    prev_gen(1:initPars.nPopulation) = organism(1,:);
    
    %externalLoad = generation .* 0.03 .* [-1 1 -1];
    %what is this???
    plottings(generation-1,:)= organism(1,1);
    
    % Elite
    elite(1:initPars.nElite) = prev_gen(1:initPars.nElite);
    
    % Mutation
    nMutation = initPars.nMutate;
    mStart = tic;
    %      if useParallel
    %          parfor i = 1:initPars.nMutation
    %              mut_rand = rand;
    %              mutOrg = find(mut_rand <= cumsum(pdf_mut_mix),1);
    %              mutate(i) = mutation(prev_gen(mutOrg),useStability,externalLoad,externalLoadPosition,useObstacles,obstacles);
    %          end
    %      else
    i = 1;
    while i <= nMutation
        mut_rand = rand;
        mutOrg = find(mut_rand <= cumsum(pdf_mut_mix),1);
        mutatedgene=mutation_m(prev_gen(mutOrg).gene);
        mutate(i).coordVec = buildMethodTest(mutatedgene);
        [allright,maxStress,maxDisplacement,num_con] = checkOrgStability(mutate(i),initPars);
        %             [allright,maxStress,maxDisplacement,num_con] = checkstability_m(mutate(i).gene,mutate(i).coordVec,initPars.externalLoad,initPars.destination,algo.useObstacles,obstacles);
        mutate(i).maxStress = maxStress;
        mutate(i).maxDisplacement = maxDisplacement;
        mutate(i).numCon = num_con;
        mutate(i).gene = mutatedgene;
        if allright ~= 1
            continue;
        end
        i = i+1;
    end
    
    mEnd = toc(mStart);
    mTime = mTime + mEnd;
    
    % Cross over
    cStart = tic;
    j = 1;
    while j <= initPars.nCrossover
        A_rand = rand;
        B_rand = rand;
        objA = find(A_rand <= cumsum(pdf_mut_mix),1);        % evolving
        objB = find(B_rand <= cumsum(pdf_mut_mix),1);        % providing elemets to A
        covergene = mix_m(prev_gen(objA).gene,prev_gen(objB).gene);
        %            crossover(j)=buildMethodOld(covergene,obstacles,initPars.externalLoad);
        crossover(j).coordVec=buildMethodTest(covergene);
        %                [stable,maxStress,maxDisplacement,numCon] = checkstability_m(crossover(j).gene,crossover(j).coordVec,initPars.externalLoad,initPars.destination,algo.useObstacles,obstacles);
        [allright,maxStress,maxDisplacement,num_con] = checkOrgStability(crossover(j),initPars);
        crossover(j).maxStress = maxStress;
        crossover(j).maxDisplacement = maxDisplacement;
        crossover(j).numCon = num_con;
        crossover(j).gene = covergene;
       
        if allright ~= 1
            continue;
        end
        j = j+1;
    end
    
    cEnd = toc(cStart);
    cTime = cTime + cEnd;
    crossover(1:initPars.nCrossover) = prev_gen(1:initPars.nCrossover);
    organism(2,:) = [elite, mutate, crossover];
    organism(2,:) = checkfitness_m(initPars.nPopulation,organism(2,:),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);

    fitnesses(generation) = fitnessfunction_m(organism(2,1),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);

    if algo.render == 1
        renderStructure(organism(2,1).coordVec,initPars.destination);
        str = sprintf('Fittest specimen | %d th Generation | Fitness: %f',generation,fitnesses(generation));
        title(str)
    end

    
    iEnd = toc(iStart);
    iTime = iTime + iEnd;
    cEnd=1;
%    disp([num2str(generation),'/',num2str(initPars.maxGen),'   ',num2str(fitnesses(generation),'%7.4f'),'    Mutation: ',num2str(mEnd,'%6.2f'),' Crossover: ',num2str(cEnd,'%6.2f'),' Iteration: ',num2str(iEnd,'%6.2f')]);

    
    organism(1,:)=organism(2,:);
    generation = generation + 1;
    
    %renderStructure(organism(generation,i),initPars.destination);
    
    
    if generation > initPars.maxGen
        break;
    end
    
end

plottings(generation-1,:)= organism(1,1);
a = memory;
disp (['Total Run Time: ',num2str(iTime,'%6.2f'),'   Mutation: ',num2str(mTime,'%6.2f'),'   Cross over: ',num2str(cTime,'%6.2f'), 'Fittest specimen: ', num2str(fitnesses(end)), ' Memory used ', num2str(a.MemUsedMATLAB) ]);
%disp (['Similarity: ',num2str(initPars.similarity), ' Total Run Time: ',num2str(iTime,'%6.2f'), ' Fittest specimen: ', num2str(fitnesses(end))]);
generations=initPars.maxGen;
%makeEvolutionMovie_m(organism,initPars,initPars.destination,obstacles)
