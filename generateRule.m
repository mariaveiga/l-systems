function rule = generateRule()
% Generate random rule for the 5 instructions we have
numRules=5;

rule(1).before='1';
rule(2).before='2';
rule(3).before='3';
rule(4).before='4';
rule(5).before='5';

rule(1).after='1';
rule(2).after='2';
rule(3).after='3';
rule(4).after='4';
rule(5).after='5';

for i=1:numRules
    for j=1:numRules
        transform = randi([1 numRules]);
        rule(i).after = strcat(rule(i).after,num2str(transform));
    end
end

%remove the zeros
for i=1:numRules
        rule(i).after = strrep(rule(i).after,'0','');
        if isempty(rule(i).after)==1
            rule(i).after = rule(i).before;
        end
end
 
% for i=1:numRules
%         rule(i).after = strrep(rule(i).after,'22','2');
%         rule(i).after = strrep(rule(i).after,'33','3');
%         rule(i).after = strrep(rule(i).after,'44','4');
%         rule(i).after = strrep(rule(i).after,'55','5');
% end

%rules = struct('before', rule.before, 'after', rule.after);
end