function CrossedSetRules = crossRules(setRules1, setRules2)
% Pick which rules to cross over

nSet1 = size(setRules1,2);
nSet2 = size(setRules2,2);

nChanges = randi([1 nSet1]);

whichChanges = datasample(1:nSet1,nChanges,'Replace',false); % can't choose rule twice...

CrossedSetRules=setRules1;

for i = 1:nChanges
   rule1=setRules1(whichChanges(i)).after;
   rule2=setRules2(randi(nSet2)).after;
   CrossedSetRules(whichChanges(i)).after = crossOverRule(rule1,rule2);
end

end