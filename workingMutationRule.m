function rule = workingMutationRule(initRule)
% Mutation operation for L-systems

% Biased mutation option:
% Introduce a probability of adding a cube everytime a mutation occurs
biasedgrowth = 1; %on = 1 | off = 0
probability = 0.4;

rule(1).before = initRule(1).before ;
rule(2).before = initRule(2).before;
rule(3).before = initRule(3).before;
rule(4).before = initRule(4).before;
rule(5).before= initRule(5).before;

for i = 1:5
    rule(i).after=initRule(i).after;
end

% Select number of rules to change:
numRules=randi(5);

% Select which rules to change
for i=1:numRules
    whichRule(i)=randi(5);
end

% Select the mutation:
% Change number (2)
% Add/Remove number (3/4)
for i=1:length(whichRule)
    cases=randi(3,1);
    if cases==1 %change a number
        j=randi(length(initRule(whichRule(i)).after)); %which position to change
        rule(whichRule(i)).after(j)=num2str(randi(5));
        
    elseif cases==2 %add a number
        j=randi(length(initRule(whichRule(i)).after)); %which position to change
        s2=num2str(randi(5));
        rule(whichRule(i)).after=strcat(rule(whichRule(i)).after(1:j),s2,rule(whichRule(i)).after(j:end));
        
    elseif cases==3 %remove a number
        j=randi(length(initRule(whichRule(i)).after)); %which position to change
        rule(whichRule(i)).after(j)=num2str(0);
        
    end
    
    if biasedgrowth == 1
        growthp=rand();
        if growthp<=probability
        j=randi(length(initRule(whichRule(i)).after)); %which position to change
        s2=num2str(1);
        rule(whichRule(i)).after=strcat(rule(whichRule(i)).after(1:j),s2,rule(whichRule(i)).after(j:end));            
        end
    end
    
end